<?php
/**
 * Handel all the admin ajax calls.
 */
class Conquisitio_Admin_Ajax {
  
  /**
   * Init all we need for ajaxing away!
   */
  public function init() {
    // add ajax call scripts
    add_action('admin_enqueue_scripts', array($this, 'add_scripts'));
    
    // ajax action actions
    add_action('wp_ajax_conquisitio_ping', array($this, 'ping'));
    add_action('wp_ajax_conquisitio_index_all', array($this, 'index_all'));
    add_action('wp_ajax_conquisitio_delete_index_all', array($this, 'delete_index_all'));
  }
  
  public function add_scripts() {
    wp_enqueue_script('conquisitio-admin-ajax', CONQUISITIO_URL .'/js/conquisitio-admin-ajax.js', array('jquery'));
    wp_localize_script('conquisitio-admin-ajax', 'conquisitio', array('ajax_url' => admin_url('admin-ajax.php'), 'plugin_url' => CONQUISITIO_URL));
  }
  
  /* Ajax call functions below this */
  
  /**
   * Pings the server, check if it running, uses unsaved settings posted to not confuse the user.
   * @return type
   */
  public function ping() {
    $settings = array (
      'hostname' => $_POST['hostname'],
      'port' => $_POST['port'],
      'path' => $_POST['path'],
    );
    
    require_once(CONQUISITIO_PATH . '/class/class-conquisitio-solarium.php');
    try {
      $solarium = new Conquisitio_Solarium($settings);
      if (in_array('', $settings)) {
        print json_encode(array('status' => FALSE));
        die();
      }
      $solarium->ping();
      print json_encode(array('status' => TRUE));
    }
    catch (Exception $e) {
      print json_encode(array('status' => FALSE));
    }
    die();
  }
  
  /**
   * Index everything.
   */
  public function index_all() {
    
    $batch_size = $_POST['batch_size'];
    $batch_number = $_POST['batch_number'];
    require_once(CONQUISITIO_PATH . '/class/class-conquisitio-solarium.php');
    try {
      $solarium = new Conquisitio_Solarium();
      $done = $solarium->index_batch($batch_number, $batch_size);
      print json_encode(array('done' => $done, 'status' => TRUE));
    }
    catch (Exception $e) {
      print json_encode(array('status' => FALSE));
    }
    die();
  }

  /**
   * Delete everything.
   */
  public function delete_index_all() {
    require_once(CONQUISITIO_PATH . '/class/class-conquisitio-solarium.php');
    try {
      $solarium = new Conquisitio_Solarium();
      $solarium->delete_all();
      print json_encode(array('status' => TRUE));
    }
    catch (Exception $e) {
      print json_encode(array('status' => FALSE));
    }
    die();
  }
}