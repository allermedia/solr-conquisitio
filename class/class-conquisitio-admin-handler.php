<?php
/**
 * Admin handler for Conquisitio.
 *
 * Generates admin pages, handles forms that are about to be saved etc.
 */
class Conquisitio_Admin_Handler extends Conquisitio_Basics
{
  private $wpdb;
  
  /**
   * Trigger pretty much whatever needs to be done in admin.
   */
  public function init() {
    global $wpdb;
    
    $this->wpdb = $wpdb;
    
    add_action('admin_menu', array($this, 'add_admin_pages'));
    add_action('wp_dashboard_setup', array($this, 'add_dashboard_widget'));
    add_action('publish_post', array($this, 'index_post'));
    
    // Admin Ajax.
    require_once(CONQUISITIO_PATH . '/class/class-conquisitio-admin-ajax.php');
    $ajax = new Conquisitio_Admin_Ajax();
    $ajax->init();
  }
  
  /**
   * Add admin pages for WordPress admin.
   */
  public function add_admin_pages() {
    add_menu_page(__('Conquisitio general settings'), __('Solr Conquisitio'),
      'manage_options', 'conquisitio-general-settings', array($this, 'render_general_settings_page'));
    add_submenu_page('conquisitio-general-settings', __('Conquisitio Index Settings'), __('Index'),
      'manage_options', 'conquisitio-index-settings', array($this, 'render_index_settings_page'));
    add_submenu_page('conquisitio-general-settings', __('Conquisitio Search Settings'), __('Search'),
      'manage_options', 'conquisitio-search-settings', array($this, 'render_search_settings_page'));
// Not yet implemented.
//    add_submenu_page('conquisitio-general-settings', __('Conquisitio Facets'), __('Facets'),
//      'manage_options', 'conquisitio-facet-settings', array($this, 'render_facet_settings_page'));
  }
  
  /**
   * Render general settings page for WordPress admin.
   */
  public function render_general_settings_page() {
    $nonce = $this->_generate_nonce('general');
    
    if (!empty($_POST['submit'])) {
      $this->_save_general_settings($_POST, $nonce);
    }
    
    $settings = $this->get_general_settings();
    extract($settings);
    
    $hostname = isset($host_settings['hostname']) ? $host_settings['hostname'] : '';
    $port = isset($host_settings['port']) ? $host_settings['port'] : '';
    $path = isset($host_settings['path']) ? $host_settings['path'] : '';
    
    include(CONQUISITIO_PATH . '/templates/admin-general-settings.php');
  }
  
  /**
   * Render index settings page for WordPress admin.
   */
  public function render_index_settings_page() {
    $nonce = $this->_generate_nonce('index');
    
    if (!empty($_POST['submit'])) {
      $this->_save_index_settings($_POST, $nonce);
    }
    
    $settings = $this->get_index_settings();
    $post_types = get_post_types(array(), 'objects');
    $custom_fields = $this->_get_all_custom_fields();
    
    include(CONQUISITIO_PATH . '/templates/admin-index-settings.php');
  }
  
  /**
   * Render search settings page for WordPress admin.
   */
  public function render_search_settings_page() {
    $nonce = $this->_generate_nonce('search');
    
    if (!empty($_POST['submit'])) {
      $this->_save_search_settings($_POST, $nonce);
    }
        
    $default_fields = array('id', 'permalink', 'wp', 'numcomments', 'title', 'content',
      'author', 'author_s', 'type', 'date', 'modified', 'displaydate', 'displaymodified',
      'categories', 'tags');
    $default_fields = apply_filters('conquisitio_default_fields', $default_fields);
    $indexed_fields = $this->get_index_settings();
    $settings = $this->get_search_settings();
    
    include(CONQUISITIO_PATH . '/templates/admin-search-settings.php');
  }
  
  /**
   * Render facets settings page for WordPress admin.
   */
  public function render_facet_settings_page() {
    include(CONQUISITIO_PATH . '/templates/admin-facet-settings.php');
  }
  
  /**
   * Add dashboard widget, with nice thingies like index on demand.
   */
  public function add_dashboard_widget() {
    if (!current_user_can('manage_options')) {
      return;
    }
    
    wp_add_dashboard_widget('conquisitio-dashboard-widget', __('Conquisitio'),
      array($this, 'render_dashboard_widget'));
  }
  
  /**
   * Render dashboard widget.
   */
  public function render_dashboard_widget() {
    include(CONQUISITIO_PATH . '/templates/admin-dashboard-widget.php');
  }
  
  /**
   * Indexes new or edited post.
   * @param int $post_id
   * @param bool $future called from a scheduled publish or not.
   */  
  public function index_post($post_id, $future = false) {
    if((isset($_POST['post_status']) && $_POST['post_status'] == 'publish') || $future) {
      require_once(CONQUISITIO_PATH . '/class/class-conquisitio-solarium.php');
      try {
        $solarium = new Conquisitio_Solarium();
        $solarium->index_single(get_post($post_id));
      } 
      catch (Exception $e) {
        trigger_error($e, E_USER_WARNING);
      }
    }
  }
  
  
  /**
   * Generates nice nonce action and field. Just provide with section, for instance
   * general, index, etc, and this does the job.
   *
   * @param string $section
   *   String of a single word that describes section. Do it right or it will break,
   *   and you will be punished with a whip over a slow fire.
   * @return array
   *   Array for nonce, field and action.
   */
  private function _generate_nonce($section) {
    $nonce = array("field" => "{$this->options_prefix}{$section}_settings_nonce_field",
      "action" => "{$this->options_prefix}{$section}_settings");
    
    return $nonce;
  }
  
  /**
   * Save general settings, probably from POST.
   *
   * @param array $data
   * @param array $nonce
   *   Nonce field and action, or it wont work.
   */
  private function _save_general_settings($data, $nonce) {
    $this->_check_nonce_field($nonce);
    
    $save = array(
      'host_settings' => array(
        'hostname' => $data['hostname'],
        'port' => $data['port'],
        'path' => $data['path']
      ),
      'performance_settings' => array(
        'commit' => isset($data['commit']) ? TRUE : FALSE
      )
    );
    
    $this->_save('general', $save);
  }
  
  /**
   * Save index settings, probably from POST.
   *
   * @param array $data
   * @param array $nonce
   */
  private function _save_index_settings($data, $nonce) {
    $this->_check_nonce_field($nonce);
    
    $save = array();
    $save['post_type'] = isset($data['post_types']) ? $data['post_types'] : array();
    $save['custom_field_type'] = isset($data['custom_field_types']) ? $data['custom_field_types'] : array();
    $save['index_comments'] = isset($data['index_comments']) ? TRUE : FALSE;
    
    $this->_save('index', $save);
  }
  
  /**
   * Save search settings to database.
   *
   * @param array $data
   *   Data to handle and store, probably from POST.
   * @param array $nonce
   */
  private function _save_search_settings($data, $nonce) {
    $this->_check_nonce_field($nonce);
    
    $save = array();
    foreach($data['fields'] as $field) {
      $data[$field] = isset($data[$field]) ? str_replace(',', '.', $data[$field]) : '';
      $save[$field] = is_numeric($data[$field]) ? floatval($data[$field]) : '';
    }
    
    $this->_save('search', $save);
  }
  
  /**
   * General function to check nonce field, as we do same thing over and over again.
   *
   * Kills if nonce isnt allright.
   *
   * @param array $nonce
   *   Information about nonce field.
   * @param string $message
   *   If we want to write custom message.
   */
  private function _check_nonce_field($nonce, $message = NULL) {
    $message = (isset($message) && !empty($message)) ? $message :
      __('Oups, dont\'t think that you are supposed to be here.');
      
    if (!isset($_POST[$nonce['field']]) || !wp_verify_nonce($_POST[$nonce['field']], $nonce['action'])) {
      wp_die($message);
    }
  }
  
  /**
   * General function to save stuff, as we do pretty much the same thing every time.
   *
   * @param string $section
   *   Seaction, for instance index, search or general.
   * @param array $save
   *   Data ready for save.
   * @return boolean
   *   TRUE if success, FALSE otherwise. Notice that these only says if something was
   *   saved/changed or not, not if something actually went wrong.
   */
  private function _save($section, $save = array()) {
    // Without section bad things can happen.
    if (empty($section)) {
      wp_die(__('Something went wrong!'));
    }
    
    $options_section = sprintf('%s%s_settings', $this->options_prefix, $section);
    if (update_option($options_section, $save)) {
      add_action('conquisitio_admin_notices', array($this, 'wp_update_notice'));
      return TRUE;
    } else {
      add_action('conquisitio_admin_notices', array($this, 'wp_error_notice'));
      return FALSE;
    }
  }
  

  /**
   * Get all custom fields without underline as prefix.
   * 
   * @return array
   *   List of all custom fields, as objects (wp default).
   */
  private function _get_all_custom_fields() {
    $sql = "SELECT DISTINCT meta_key
      FROM {$this->wpdb->prefix}postmeta
      WHERE meta_key NOT LIKE '\_%'";
    $result = $this->wpdb->get_results($sql);
    
    return $result;
  }
}