<?php
/**
 * Sets up the very foundation of Conquisitio, such as options prefix, variables
 * that are used in all classes etc.
 */
class Conquisitio_Basics
{
  public $options_prefix;
  
  /**
   * Constructor, doing the things that we always need.
   */
  public function __construct() {
    $this->options_prefix = 'conquisitio_';
  }
  
  /**
   * Get general settings from database. So not everyone need to keep track of
   * the names of all options.
   *
   * @return array
   */
  public function get_general_settings() {
    $settings = get_option($this->options_prefix . 'general_settings', array());
    
    return $settings;
  }
  
  /**
   * Get index settings from database.
   *
   * @return array
   */
  public function get_index_settings() {
    $settings = get_option($this->options_prefix . 'index_settings',
      array('post_type' => array(), 'custom_field_type' => array()));
    
    return $settings;
  }
  
  /**
   * Get search settings from database. Query fields and weight. We assume that
   * we always use dismax.
   *
   * @return array
   */
  public function get_search_settings() {
    $settings = get_option($this->options_prefix . 'search_settings',
      array());
    
    return $settings;
  }
  
  /**
   * WordPress update notice.
   */
  public function wp_update_notice() {
    include(CONQUISITIO_PATH . '/templates/admin-update-notice.php');
  }
  
  /**
   * WordPress error notice.
   */
  public function wp_error_notice() {
    include(CONQUISITIO_PATH . '/templates/admin-error-notice.php');
  }
}
