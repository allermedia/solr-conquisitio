<?php
require_once(CONQUISITIO_PATH . '/class/class-conquisitio-basics.php');
require_once(CONQUISITIO_PATH . '/class/class-conquisitio-admin-handler.php');
require_once(CONQUISITIO_PATH . '/class/class-conquisitio-search.php');

/**
 * Main handler of Conquisitio. This class is always triggered, and controls
 * whatever should be done with Conquisitio.
 */
class Conquisitio_Main_Handler extends Conquisitio_Basics
{
  protected $conquisitio_admin_handler;
  protected $conquisitio_search;
  
  /**
   * Things that need to be set.
   */
  public function __construct() {
    // Nothing to do here yet, changed me mind.
    
    parent::__construct();
  }
  
  /**
   * Start application, run Forrest run!
   */
  public function init() {
    if (is_admin()) {
      $this->conquisitio_admin_handler = new Conquisitio_Admin_Handler();
      $this->conquisitio_admin_handler->init();
    } else {
      try {
        $this->conquisitio_search = new Conquisitio_Search();
        add_action('pre_get_posts', array($this->conquisitio_search, 'alter_main_query'));
        add_action('wp', array($this->conquisitio_search, 'clean_up_main_query'));
      } catch (Exception $e) {
        trigger_error($e, E_USER_WARNING);
      }
    }
    add_action('publish_future_post', array($this, 'index_future_post'));
  }

  /**
   * Workaround to not instansiate an admin handler only when scheduled post is commited to solr
   * @param int post_id scheduled post id.
   */
  public function index_future_post($post_id) {
    $this->conquisitio_admin_handler = new Conquisitio_Admin_Handler();
    $this->conquisitio_admin_handler->init();
    $this->conquisitio_admin_handler->index_post($post_id, true);
  }
}
