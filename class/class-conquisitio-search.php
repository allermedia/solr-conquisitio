<?php
require_once(CONQUISITIO_PATH . '/class/class-conquisitio-solarium.php');

/**
 * Handle actual search part of Conquisitio. This tells WordPress what to load,
 * and how.
 */
class Conquisitio_Search
{
  protected $resultset;
  protected $real_paged;
  protected $posts_per_page;
  private $_solarium;
  private $_this_query;
  
  /**
   * Set up all thingies that we need.
   */
  public function __construct() {
    $this->_solarium = new Conquisitio_Solarium();
    $this->_this_query = FALSE;
    
  }
  
  /**
   * Hook up and alter WordPress main query, before first time run.
   *
   * @param object $query
   *   WordPress query object.
   * @return object
   *   WP query object as we want it.
   */
  public function alter_main_query($query) {
    global $wp_the_query;

    $use_wp_search = false;
    $use_wp_search = apply_filters('conquisitio_use_wp_search', $use_wp_search);
    if ($use_wp_search) {
      return $query;
    }
    
    if ((!is_home() && !is_search()) || $wp_the_query != $query) {
      $this->_this_query = FALSE;
      return $query;
    } else {
      $this->_this_query = TRUE;
    }
    
    $options = get_option('solum_options', array('solum_posts_search' => get_option('posts_per_page', 10)));
    $this->posts_per_page = $options['solum_posts_search'];
    
    // If you want a custom default search query, other than just empty.
    $default_search_query = '';
    $default_search_query = apply_filters('conquisitio_default_query', $default_search_query);
   
 
    $search = $query->query_vars['s'];
    if ($search == $default_search_query) {
      $query->set('s', '');
      $search = '';
    }
    
    // Fix pagination issue
    $this->real_paged = $query->query_vars['paged'];
    
    $offset = ($this->real_paged-1) * $this->posts_per_page;
    if ($offset <= 0) {
      $offset = 0;
    }
    $this->resultset = $this->_solarium->select($search, array('offset' => $offset, 'rows' => $this->posts_per_page));
    
    $wp_ids = array();
    foreach($this->resultset as $document) {
      foreach($document as $field => $value) {
        $wp_ids[] = $value;
      }
    }
    // Fix issue with wordpress trying to search
    $query->set('s', '');
    
    // Tell wordpress which posts to pick, annoying, if no wp_ids is provided, wordpress thinks it can have it all
    // Set id to something that dont exist to stop getting posts if nothing is returned.
    if(count($wp_ids) == 0) {
       $wp_ids[] = -1;
    }
    
    $query->set('post__in', $wp_ids);
    // Not sticky posts though...
    $query->set('ignore_sticky_posts', 1);
    // ...and tell wordpress which order to present posts in
    $query->set('orderby', 'post__in');
    
    // Pagination issue part 2
    $query->set('paged', 1);
    $query = apply_filters('conquisitio_alter_wp_query', $query);
    
    return $query;
  }
  
  /**
   * Do some cleanup on main query, after it has been set up.
   *
   * @param object $query
   * @return object
   */
  public function clean_up_main_query($query) {
    global $wp_query, $paged;
    
    if (!$this->_this_query) {
      return $query;
    }
    
    $wp_query->found_posts = $this->resultset->getNumFound();
    $wp_query->max_num_pages = ceil($wp_query->found_posts / $this->posts_per_page);
    $wp_query->query_vars['paged'] = $this->real_paged;
    $paged = $this->real_paged;
  }
}
