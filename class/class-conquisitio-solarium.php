<?php
/**
 * Handle the Solarium calls.
 * 
 */

// Init the Solarium lib.
require_once(CONQUISITIO_PATH . '/lib/solarium/vendor/autoload.php');

class Conquisitio_Solarium extends Conquisitio_Basics {
  protected $host_settings;
  protected $index_settings;
  protected $performance_settings;
  protected $client;
  
/**
 * Set the client based on saved settings.
 * @param type $settings
 * @throws Exception
 */
  function __construct($host_settings = FALSE, $index_settings = FALSE) {
    parent::__construct();
    $settings = $this->get_general_settings();
    if (count($settings) > 0 || $host_settings != FALSE) {
      if (!$host_settings) {
        $host_settings = $settings['host_settings'];
      }

      $this->performance_settings = $settings['performance_settings'];

      if (count($host_settings) > 0) {
        $this->host_settings = array(
          'endpoint' => array(
            'localhost' => array(
              'host' => $host_settings['hostname'],
              'port' => $host_settings['port'],
              'path' => $host_settings['path'],
            )
          )
        );
        $this->client = new Solarium\Client($this->host_settings);
      }
      else {
        throw new Exception('No host settings available!');
      }
    }
    else {
      throw new Exception('No general settings available!');
    }
    
    
    if(!$index_settings) {
      $this->index_settings = $this->get_index_settings();
    }
    else {
      $this->index_settings = $index_settings;
    }
  }
  
  /**
   * Ping the solr server.
   * @return boolean success or not
   */
  public function ping() {
    $ping = $this->client->createPing();
    // Doesn´t matter if I try to catch here but the exception thrown is very clear anyhow!!
    $this->client->ping($ping);
    return true;
  }
  
  /**
   * Delete all documents.
   */
  public function delete_all() {
    $update = $this->client->createUpdate();

    // add the delete query and a commit command to the update query
    $update->addDeleteQuery('*:*');
    $update->addCommit();

    // this executes the query and returns the result
    $this->client->update($update);
  }
  
  /**
   * Index single post.
   * @param object $post
   */
  public function index_single($post) {
    $update = $this->client->createUpdate();
    
    //  Only if settings allow it
    if (in_array($post->post_type, $this->index_settings['post_type'])) {
      $document = $this->build_document($post, $update);

      $update->addDocuments(array($document));
      if($this->performance_settings['commit']) {
        $update->addCommit();
      }

      $this->client->update($update);
    }
  }
  
  /**
   * Index in a batch according to settings.
   * @return boolean If a full batch is indexed or not.
   */
  public function index_batch($offset, $batch_size = 100) {
    $update = $this->client->createUpdate();
    
    $args = array(
      'numberposts' => $batch_size,
      'offset' => $offset * $batch_size,
      'post_status' => 'publish',
      'post_type' => $this->index_settings['post_type']
    );
    $posts = get_posts($args);
    
    
    $documents = array();
    foreach ($posts as $post) {
      $documents[] = $this->build_document($post, $update);
    }
    
    $update->addDocuments($documents);
    $update->addCommit();
    
    $this->client->update($update);
    
    return count($posts) != $batch_size;
  }
  
  
  /**
   * Index everything according to settings.
   */
  public function index_all() {
    $update = $this->client->createUpdate();
    
    $args = array(
      'numberposts' => -1,
      'post_status' => 'publish',
      'post_type' => $this->index_settings['post_type']
    );
    $posts = get_posts($args);
    
    
    
    $documents = array();
    foreach ($posts as $post) {
      $documents[] = $this->build_document($post, $update);
    }
    
    $update->addDocuments($documents);
    $update->addCommit();
    
    $this->client->update($update);
  }
  
  
  /**
   * Build document.
   * @param object $post
   * @param object $update
   * @return object the document
   */
  public function build_document($post, $update){
    $document = $update->createDocument();
    
    $author = get_userdata($post->post_author);
    
    $document->setField('id', $post->ID);
    $document->setField('permalink', get_permalink($post->ID));
    // Not sure why!?
    $document->setField('wp', 'wp');
		$document->setField('numcomments', $post->comment_count);
		$document->setField('title', $post->post_title);
		$document->setField('content', strip_tags($post->post_content));
		$document->setField('author', $author->display_name);
		$document->setField('author_s', get_author_posts_url($author->ID, $author->user_nicename));
		$document->setField('type', $post->post_type);
		$document->setField('date', $this->_mysql_to_solr_datetime($post->post_date_gmt));
		$document->setField('modified', $this->_mysql_to_solr_datetime($post->post_modified_gmt));
		$document->setField('displaydate', $post->post_date);
		$document->setField('displaymodified', $post->post_modified);


		if ($this->index_settings['index_comments']) {
      $comment_args = array (
        'post_id' => $post->ID,
        'status' => 'approve',
      );
      $comments = get_comments($comment_args);
      foreach ($comments as $comment) {
        $document->addField('comments', $comment->comment_content);
      }
		}
    
    
    $categories = get_the_category($post->ID);
		if ($categories) {
			foreach($categories as $category) {
        $document->addField('categories', $category->cat_name);
			}
		}

		$tags = get_the_tags($post->ID);
		if ($tags) {
			foreach( $tags as $tag ) {
				$document->addField('tags', $tag->name);
			}
		}
    
		$index_custom_fields = $this->index_settings['custom_field_type'];
    $post_custom_fields = get_post_custom($post->ID);
    
    foreach($index_custom_fields as $custom_field_name) {
      if (isset($post_custom_fields[$custom_field_name])) {
        foreach($post_custom_fields[$custom_field_name] as $post_custom_value) {
          $solr_field_name =  strtolower(str_replace(' ', '_', $custom_field_name));
          $document->addField($solr_field_name . '_str', $post_custom_value);
          $document->addField($solr_field_name . '_srch', $post_custom_value);
        }
      }
    }
    
    return $document;
  }
  
  
  
  /**
   * Perform a select search.
   *
   * @param string $search
   * @return object
   *   Resultset from Solarium
   */
  public function select($search, $args = array()) {
    $fields = array('id');
    
    // Create a select query instance
    $query = $this->client->createSelect();
    $query->setFields(implode('', $fields));
    
    if (isset($args['offset'])) {
      $query->setStart($args['offset']);
    }
    
    if (isset($args['rows'])) {
      $query->setRows($args['rows']);
    }
    
    // Get dismax component, and set up query fields
    $dismax = $query->getDisMax();
    $dismax = apply_filters('conquisitio_alter_dismax_object', $dismax);
    $queryfields = $this->_generate_queryfields();
    if (!empty($queryfields)) {
      $dismax->setQueryFields(implode(' ', $queryfields));
    }
    
    // Finally, do whatever you like to query... (like filter-query etc). Use with care.
    $query = apply_filters('conquisitio_alter_query', $query);
    
    // Set actual query
    $query->setQuery($search);
    
    // Execute query
    $resultset = $this->client->select($query);
    return $resultset;
  }
  
  private function _mysql_to_solr_datetime($thedate) {
    $date_regex = '/(\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2})/';
    $replace_string = '${1}T${2}Z';
    return preg_replace($date_regex, $replace_string, $thedate);
  }
  
  /**
   * Generate qf from db.
   *
   * @return array
   *   Array of qf, ready for solarium to use.
   */
  private function _generate_queryfields() {
    $qf = array();
    
    $fields = $this->get_search_settings();
    foreach($fields as $field => $weight) {
      $qf[] = !empty($weight) ? sprintf('%s^%s', $field, $weight) : $field;
    }
    
    $qf = apply_filters('conquisitio_queryfields', $qf);
    
    return $qf;
  }
}