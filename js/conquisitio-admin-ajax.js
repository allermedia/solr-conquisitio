var batch_number = 0;
var index_in_progress = false;
var batch_size;
jQuery(document).ready(function() {
  jQuery('.ping').click(function() {
    jQuery('#ping-response').html('');
    jQuery('.ping-loading').show();
    call({
      action: 'conquisitio_ping',
      hostname: jQuery('#hostname').val(),
      port: jQuery('#port').val(),
      path: jQuery('#path').val()
    }, ping_success);
  });
  
  jQuery('#index').click(function() {
    if (!isPositiveInteger(jQuery('#batch-size').val())) {
      jQuery('.batch-size-error').show();
      return false;
    }
    else {
      jQuery('.batch-size-error').hide();
    }
    
    if (!index_in_progress) {
      index_in_progress = true;
      jQuery('#index-response').html('');
      jQuery('.index-loading').show();
      batch_size = jQuery('#batch-size').val();
      call({
        action: 'conquisitio_index_all',
        batch_size: batch_size,
        batch_number: batch_number
      }, index_all_success);
    }
  });
  
  jQuery('.delete-index').click(function() {
    jQuery('#delete-index-response').html('');
    jQuery('.delete-index-loading').show();
    call({
      action: 'conquisitio_delete_index_all'
    }, delete_index_all_success);
  });
});


/**
 * Helper function for our ajax calls.
 * @param object data
 * @param function callback
 */
function call(data, callback) {
  jQuery.ajax({
    type: "POST",
    url: conquisitio.ajax_url,
    data: data,
    dataType: 'json',
    success: callback
  });
}

/**
 * Ping success callback function.
 * Shows icon for success or fail.
 * @param object data
 */
function ping_success(data) {
  jQuery('.ping-loading').hide();
  if (data.status) {
    jQuery('#ping-response').html('<img src="' + conquisitio.plugin_url + '/images/tick.png" alt="Ping success" />')
  }
  else {
    jQuery('#ping-response').html('<img src="' + conquisitio.plugin_url + '/images/cross.png" alt="Ping failed" />')
  };
}

/**
 * Index success callback function.
 * 
 * @param object data
 */
function index_all_success(data) {
  if (data.status) {
    // Not done, do another batch!
    if (!data.done) {
      batch_number++;
      call({
        action: 'conquisitio_index_all',
        batch_size: batch_size,
        batch_number: batch_number
      }, index_all_success);
    }
    else {
      batch_number = 0;
      index_in_progress = false;
      jQuery('.index-loading').hide();
      jQuery('#index-response').html('<img src="' + conquisitio.plugin_url + '/images/tick.png" alt="Ping success" />')
    }
  }
  else {
    batch_number = 0;
    index_in_progress = false;
    jQuery('.index-loading').hide();
    jQuery('#index-response').html('<img src="' + conquisitio.plugin_url + '/images/cross.png" alt="Ping failed" />')
  };
    
  
}

/**
 * Delete index success callback function.
 * 
 * @param object data
 */
function delete_index_all_success(data) {
  jQuery('.delete-index-loading').hide();
  if (data.status) {
    jQuery('#delete-index-response').html('<img src="' + conquisitio.plugin_url + '/images/tick.png" alt="Ping success" />')
  }
  else {
    jQuery('#delete-index-response').html('<img src="' + conquisitio.plugin_url + '/images/cross.png" alt="Ping failed" />')
  };
}

/**
 * Check if integer.
 * @param sting number
 * @returns boolean if numeric or not.
 */
function isPositiveInteger(input) {
  var intRegex = /^\d+$/;
  return intRegex.test(input);
}