<?php
/**
 * Plugin Name: Solr Conquisitio
 * Description: Replace WordPress default search with an advanced Solr search, with support for facets etc. This plugin requires WordPress 3.5 or later to work properly.
 * Version: 1.1.0
 * Author: Johannes Henrysson <johannes.henrysson@aller.se>, Joel Bernerman <joel.bernerman@aller.se>, Aller Media AB
 * Author URI: http://aller.se/
 */
define('CONQUISITIO_PATH', plugin_dir_path(__FILE__));
define('CONQUISITIO_URL', plugins_url('', __FILE__));

require_once(CONQUISITIO_PATH . '/class/class-conquisitio-main-handler.php');

// Init Conquisitio
$conquisitio_main_handler = new Conquisitio_Main_Handler();
$conquisitio_main_handler->init();