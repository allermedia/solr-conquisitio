<p>
  <?php _e('Create, or reindex your existing, SOLR index.'); ?>
</p>
<p class="index">
  <input type="button" name="index" id="index" class="button-primary" value="<?php _e('Index'); ?>" />
  <span id="index-response"></span><span><img src="<?php print CONQUISITIO_URL . '/images/loader.gif'; ?>" alt="loading" class="loading index-loading" style="display: none;"/></span>
  <label for="batch-size">Batch size: </label><input type="text" name="batch-size" id="batch-size" value="100" />
  <span class="batch-size-error" style="color:#f00; display: none;"><?php print __('Batch size must be a positive integer!'); ?></span>
</p>
<p class="delete-index">
  <input type="button" name="delete-index" id="delete-index" class="button-secondary" value="<?php _e('Delete index'); ?>" />
  <span id="delete-index-response"></span><span><img src="<?php print CONQUISITIO_URL . '/images/loader.gif'; ?>" alt="loading" class="loading delete-index-loading" style="display: none;"/></span>
</p>