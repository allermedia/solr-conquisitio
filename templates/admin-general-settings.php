<div class="wrap">
  <div id="icon-options-general" class="icon32"><br /></div>
  <h2><?php _e('General Settings'); ?></h2>
  
  <?php do_action('conquisitio_admin_notices'); ?>
  
  <form action="" method="POST">
    <?php wp_nonce_field($nonce['action'], $nonce['field']); ?>
    
    <h3><?php _e('Solr server configuration'); ?></h3>
    <table class="form-table">
      <tbody>
        <tr valign="top">
          <th scope="row"><label for="hostname"><?php _e('Hostname'); ?></label></th>
          <td>
            <input type="text" name="hostname" id="hostname" value="<?php print $hostname; ?>" class="regular-text" />
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><label for="port"><?php _e('Port'); ?></label></th>
          <td>
            <input type="text" name="port" id="port" value="<?php print $port; ?>" class="regular-text" />
          </td>
        </tr>
        
        <tr valign="top">
          <th scope="row"><label for="path"><?php _e('Path'); ?></label></th>
          <td>
            <input type="text" name="path" id="path" value="<?php print $path; ?>" class="regular-text" />
          </td>
        </tr>
      </tbody>
    </table> <!-- .form-table -->
    
    <h3><?php _e('Solr performance configuration'); ?></h3>
    
    <table class="form-table">
      <tbody>
        <tr valign="top">
          <th scope="row"><label for="commit"><?php _e('Commit to solr on publish.'); ?></label></th>
          <td>
            <input type="checkbox" name="commit" id="hostname" <?php print (isset($performance_settings['commit']) && $performance_settings['commit']) ? 'checked' : ''; ?> class="regular-text" />
          </td>
        </tr>
      </tbody>
    </table> <!-- .form-table -->
    
    <table>
      <tr>
        <td>
          <p class="submit">
            <input type="submit" name="submit" id="submit" class="button-primary" value="<?php _e('Save'); ?>" />
          </p>
        </td>
        <td>
          <p class="ping">
            <input type="button" name="ping" id="ping" class="button" value="<?php _e('Ping'); ?>" />
            <span id="ping-response"></span><img src="<?php print CONQUISITIO_URL . '/images/loader.gif'; ?>" alt="loading" class="loading ping-loading" style="display: none;"/>
          </p>
        </td>
      </tr>
    </table>
    
    
  </form>
</div> <!-- .wrap -->