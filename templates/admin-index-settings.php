<div class="wrap">
  <div id="icon-options-general" class="icon32"><br /></div>
  <h2><?php _e('Index Settings'); ?></h2>
  
  <?php do_action('conquisitio_admin_notices'); ?>
  
  <form action="" method="POST">
    <?php wp_nonce_field($nonce['action'], $nonce['field']); ?>
    
    <h3><?php _e('Post types'); ?></h3>
    <p class="description">
      <?php _e('Choose which post types to index.'); ?>
    </p>
    <table class="wp-list-table widefat fixed">
      <thead>
        <tr>
          <th scope="col" class="manage-column column-cb check-column">
            <input type="checkbox" />
          </th>
          <th scope="col" class="manage-column column-title sortable desc">
            <?php _e('Name'); ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($post_types as $post_type) : $i = isset($i) ? ++$i : 0; ?>
          <tr class="<?php if (($i % 2) == 0) : ?>alternate<?php endif; ?>">
            <th scope="row" class="check-column">
              <input type="checkbox" name="post_types[]" value="<?php print $post_type->name; ?>" <?php if (array_search($post_type->name, $settings['post_type']) !== FALSE) : ?>checked<?php endif; ?> />
            </th>
            <td class="name">
              <?php print $post_type->labels->name; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    
    <h3><?php _e('Custom fields'); ?></h3>
    <p class="description">
      <?php _e('Choose which custom fields to index. These custom fields are not depending on choosing above.'); ?>
    </p>
    <table class="wp-list-table widefat fixed">
      <thead>
        <tr>
          <th scope="col" class="manage-column column-cb check-column">
            <input type="checkbox" />
          </th>
          <th scope="col" class="manage-column column-title sortable desc">
            <?php _e('Name'); ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($custom_fields as $i => $custom_field) : ?>
          <tr class="<?php if (($i % 2) == 0) : ?>alternate<?php endif; ?>">
            <th scope="row" class="check-column">
              <input type="checkbox" name="custom_field_types[]" value="<?php print $custom_field->meta_key; ?>" <?php if (array_search($custom_field->meta_key, $settings['custom_field_type']) !== FALSE) : ?>checked<?php endif; ?> />
            </th>
            <td class="name">
              <?php print $custom_field->meta_key; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    
    <h3><?php _e('Various'); ?></h3>
    <p class="description">
      <?php _e('Other things that we should index, that arent really easy to categorize.'); ?>
    </p>
    <table class="wp-list-table widefat fixed">
      <thead>
        <tr>
          <th scope="col" class="manage-column column-cb check-column">
            <input type="checkbox" />
          </th>
          <th scope="col" class="manage-column column-title sortable desc">
            <?php _e('Name'); ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="alternate">
          <th scope="row" class="check-column">
            <input type="checkbox" name="index_comments" value="1" <?php if (isset($settings['index_comments']) && $settings['index_comments']) : ?>checked<?php endif; ?> />
          </th>
          <td class="name">
            <?php _e('Index comments'); ?>
          </td>
        </tr>
      </tbody>
    </table>
    
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button-primary" value="<?php _e('Save'); ?>" />
    </p>
  </form>
</div> <!-- .wrap -->