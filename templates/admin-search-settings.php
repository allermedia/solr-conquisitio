<div class="wrap">
  <div id="icon-tools" class="icon32"><br /></div>
  <h2><?php _e('Search Settings'); ?></h2>
  
  <?php do_action('conquisitio_admin_notices'); ?>
  
  <form action="" method="POST">
    <?php wp_nonce_field($nonce['action'], $nonce['field']); ?>
    
    <h3><?php _e('Default fields'); ?></h3>
    <p class="description">
      <?php _e('Choose which post types to make searchable. Only indexed fields are visible here.'); ?>
    </p>
    <table class="wp-list-table widefat fixed">
      <thead>
        <tr>
          <th scope="col" class="manage-column column-cb check-column">
            <input type="checkbox" />
          </th>
          <th scope="col" class="manage-column column-title sortable desc">
            <?php _e('Name'); ?>
          </th>
          <th scope="col" class="manage-column column-title">
            <?php _e('Weight'); ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($default_fields as $i => $field) : ?>
          <tr class="<?php if (($i % 2) == 0) : ?>alternate<?php endif; ?>">
            <th scope="row" class="check-column">
              <input type="checkbox" name="fields[]" value="<?php print $field; ?>" <?php if (isset($settings[$field])) : ?>checked<?php endif; ?> />
            </th>
            <td class="name">
              <?php print $field; ?>
            </td>
            <td class="name">
              <input type="text" value="<?php if (isset($settings[$field])) : ?><?php print $settings[$field]; ?><?php endif; ?>" name="<?php print $field; ?>" class="small-text" />
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    
    <h3><?php _e('Custom fields'); ?></h3>
    <p class="description">
      <?php _e('Choose which post types to make searchable. Only indexed fields are visible here.'); ?>
    </p>
    <table class="wp-list-table widefat fixed">
      <thead>
        <tr>
          <th scope="col" class="manage-column column-cb check-column">
            <input type="checkbox" />
          </th>
          <th scope="col" class="manage-column column-title sortable desc">
            <?php _e('Name'); ?>
          </th>
          <th scope="col" class="manage-column column-title">
            <?php _e('Weight'); ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($indexed_fields['custom_field_type'] as $i => $field) : ?>
          <tr class="<?php if (($i % 2) == 0) : ?>alternate<?php endif; ?>">
            <th scope="row" class="check-column">
              <input type="checkbox" name="fields[]" value="<?php print $field; ?>_srch" <?php if (isset($settings[$field . '_srch'])) : ?>checked<?php endif; ?> />
            </th>
            <td class="name">
              <?php print $field; ?>
            </td>
            <td class="name">
              <input type="text" value="<?php if (isset($settings[$field . '_srch'])) : ?><?php print $settings[$field . '_srch']; ?><?php endif; ?>" name="<?php print $field; ?>_srch" class="small-text"/>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button-primary" value="<?php _e('Save'); ?>" />
    </p>
  </form>
  
  <?php if (!empty($indexed_fields['custom_field_type'])) : foreach($indexed_fields['custom_field_type'] as $field) : ?>
  <?php endforeach; else : ?>
    <?php _e('You have to add fields to index, via index settings page.'); ?>
  <?php endif; ?>
</div> <!-- .wrap -->